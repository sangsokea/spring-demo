FROM openjdk:11
ADD target/spring-demo-0.0.1-SNAPSHOT.jar /root.jar
ENTRYPOINT ["java", "-jar","/root.jar"]
EXPOSE 8080