package com.example.springdemo.controller;

import com.example.springdemo.repository.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

    @Autowired
    Topic topic;

    @GetMapping
    String printText(){
        return topic.printText();
    }
}
