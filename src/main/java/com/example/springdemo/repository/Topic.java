package com.example.springdemo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper

public interface Topic {
    @Select("select text from topic")
    String printText();
}
